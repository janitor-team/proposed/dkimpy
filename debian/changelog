dkimpy (1.0.5-2) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 16:59:29 -0500

dkimpy (1.0.5-1) unstable; urgency=medium

  * Bump debhelper compat to 12
  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Sat, 08 Aug 2020 23:05:48 -0400

dkimpy (1.0.4-1) unstable; urgency=medium

  * New upstream release
    - Fixes regression from 0.9 series which leads to tracebacks when
      attempting to verify messages with no dkim signature
  * Bump standards-version to 4.5.0 with no further changes

 -- Scott Kitterman <scott@kitterman.com>  Mon, 06 Apr 2020 08:25:22 -0400

dkimpy (1.0.3-1) unstable; urgency=medium

  * New upstream relaese
    - Fixes regression from 1.0.0 where multiple signature verification no
      longer worked

 -- Scott Kitterman <scott@kitterman.com>  Wed, 15 Jan 2020 11:15:47 -0500

dkimpy (1.0.2-2) unstable; urgency=medium

  * Run wrap and sort
  * Manually specify package depends instead of allowing dh-python to guess
    so DNS related package dependenncies are correct

 -- Scott Kitterman <scott@kitterman.com>  Wed, 08 Jan 2020 07:10:23 -0500

dkimpy (1.0.2-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Tue, 31 Dec 2019 01:33:21 -0500

dkimpy (1.0.1-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Sun, 15 Dec 2019 01:36:26 -0500

dkimpy (1.0.0-1) unstable; urgency=medium

  * New upstream release
  * Update debian/docs to install README.md due to upstream name change
  * Run autopkgtest for all supported python3 versions

 -- Scott Kitterman <scott@kitterman.com>  Mon, 09 Dec 2019 18:28:43 -0500

dkimpy (0.9.5-1) unstable; urgency=medium

  * New upstream release
  * Bump standards-version to 4.4.1 without further change

 -- Scott Kitterman <scott@kitterman.com>  Mon, 07 Oct 2019 08:58:45 -0400

dkimpy (0.9.4-1) unstable; urgency=medium

  * New upstream release
  * Update public upstream key to be minimal

 -- Scott Kitterman <scott@kitterman.com>  Wed, 25 Sep 2019 16:42:19 -0400

dkimpy (0.9.3-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Scott Kitterman ]
  * New upstream release
  * Update debian/copyright

 -- Scott Kitterman <scott@kitterman.com>  Fri, 09 Aug 2019 12:00:33 -0400

dkimpy (0.9.2-1) unstable; urgency=medium

  * New upstream release
  * Drop Python 2 support (no rdepends)
  * Bump standards-version to 4.4.0 without further change

 -- Scott Kitterman <scott@kitterman.com>  Fri, 12 Jul 2019 18:07:18 -0400

dkimpy (0.9.1-1) unstable; urgency=medium

  * New upstream release
  * Update debian/watch to check upstream signature

 -- Scott Kitterman <scott@kitterman.com>  Sun, 09 Dec 2018 15:33:28 -0500

dkimpy (0.9.0-1) unstable; urgency=medium

  * New upstream release
  * Bump standards-version to 4.2.1 without further change

 -- Scott Kitterman <scott@kitterman.com>  Tue, 30 Oct 2018 12:42:01 -0400

dkimpy (0.8.1-2) unstable; urgency=medium

  * Add autopkgtest using upstream test suite
  * Set buildsystem to pybuild
  * Remove obsolete fields from debian/control
  * Add suggests python/python3-authres (needed only for ARC)
  * Drop hard coded DNS depends since they are correctly generated
    during substitution variable expansion

 -- Scott Kitterman <scott@kitterman.com>  Fri, 22 Jun 2018 16:04:19 -0400

dkimpy (0.8.1-1) unstable; urgency=medium

  * Add missing depends on python/python3-pkg-resources
  * New upstream release
  * Add missing Breaks on python-dkim for python3-dkim (Closes: #901210)

 -- Scott Kitterman <scott@kitterman.com>  Sat, 16 Jun 2018 21:56:04 -0400

dkimpy (0.8.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field

  [ Scott Kitterman ]
  * New upstream release
    - Switches to setuptools, add python-setuptools and python3-setuptools to
      build-depends (Closes: #896753)
    - Clean up debian/rules since moving scripts to language neutral name is
      now done upstream
  * Bump standards-version to 4.1.4 without further change
  * Provide /usr/bin scripts from python3-dkim rather than python-dkim
    - Update package descriptions
    - Python3-dkim Replaces previous versions of python-dkim

 -- Scott Kitterman <scott@kitterman.com>  Fri, 18 May 2018 20:38:37 -0400

dkimpy (0.7.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Scott Kitterman ]
  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Sat, 17 Feb 2018 14:22:20 -0500

dkimpy (0.7.0-2) unstable; urgency=medium

  * Add missing depends on python/python3-nacl (Ed25519 verification support
    will be an RFC MUST requirement)
  * Update package descriptions to mention both RSA and Ed25519

 -- Scott Kitterman <scott@kitterman.com>  Thu, 08 Feb 2018 00:17:28 -0500

dkimpy (0.7.0-1) unstable; urgency=medium

  * New upstream release
    - Includes experimental support for new Ed25519 signatures
  * Update debian/watch to use secure URI
  * Fixup trailing whitespace
  * Bump standards-version to 4.1.3 without further change

 -- Scott Kitterman <scott@kitterman.com>  Wed, 07 Feb 2018 01:31:25 -0500

dkimpy (0.6.2-1) unstable; urgency=medium

  * New upstream release
    - Bug fixes, including Closes: #863690

 -- Scott Kitterman <scott@kitterman.com>  Sat, 10 Jun 2017 16:44:45 -0400

dkimpy (0.6.1-1) unstable; urgency=medium

  * New upstream release: (Closes: #856609, #856611, #856613)

 -- Scott Kitterman <scott@kitterman.com>  Thu, 02 Mar 2017 17:16:36 -0500

dkimpy (0.6.0-1) unstable; urgency=medium

  * New upstream release
  * Updated debian/copyright
  * Updated debian/rules to install new scripts
  * Adjusted required python versions
  * Updated package descriptions
  * Bumped debhelper version requirement and compat to 9

 -- Scott Kitterman <scott@kitterman.com>  Mon, 23 Jan 2017 14:39:08 -0500

dkimpy (0.5.6-2) unstable; urgency=medium

  [ Scott Kitterman ]
  * Clean up clean rule to fix FTBFS on second build
  * Don't install test data in pyshared for python-dkim
  * Bump standards version to 3.9.8 without further change

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)

 -- Scott Kitterman <scott@kitterman.com>  Wed, 29 Jun 2016 00:08:51 -0400

dkimpy (0.5.6-1) unstable; urgency=medium

  [ Scott Kitterman ]
  * New upstream release (LP: #1350141)
  * Remove uupdate from debian/watch, no longer relevant when using git-dpm
  * Bump standards version to 3.9.6 without further change
  * Add dh-python to build-depends

  [ SVN-Git Migration ]
  * Migrate packaging to git with git-dpm

 -- Scott Kitterman <scott@kitterman.com>  Mon, 07 Dec 2015 17:59:05 -0500

dkimpy (0.5.4-1) unstable; urgency=medium

  * Urgency medium for important bug fix

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Scott Kitterman ]
  * New upstream release
    - Fixes Gmail signature verification failures due to improper FWS regular
      expression - Thanks to Peter Palfrader (weasel) for the patch
      (Closes: #711751)
  * Drop --no-compile from debian/rules so byte compilation gets tested during
    build
  * Bump minimum dephelper version to 8.1 and debian/compat to 8 for
    build-{arch,indep} support
  * Bump standards version to 3.9.4 without further change

 -- Scott Kitterman <scott@kitterman.com>  Sun, 09 Jun 2013 22:51:55 -0400

dkimpy (0.5.3-1) unstable; urgency=medium

  * Urgency medium for multiple RC bug fixes
  * New upstream release:
  * Fix header unfolding and body hash calculation errors that cause
    correct DKIM signatures to fail to verify in many cases
    (Closes: #691663)
  * Add minimum key length requirement to prevent validation of signatures
    generated with insecure keys (Closes: #691662)

 -- Scott Kitterman <scott@kitterman.com>  Sun, 28 Oct 2012 10:32:13 +0100

dkimpy (0.5.2-1) unstable; urgency=low

  * New upstream release
    - Change canonicalization defaults to work around issues with different
      verification implementations <https://launchpad.net/bugs/939128>
    - Fully fold DKIM-Signature on sign, and ignore FWS in b= value on verify
    - Fix hashing problem while signing using sha1
  * Add alternate depends on python3-dnspython for python3-dkim since it will
    enter the archive shortly
  * Install tests directory, including test data
  * Drop obsolete breaks on Lenny dkimproxy
  * Fix debian watch to work with package rename and  Launchpad changes
  * Bump standards version to 3.9.3 without further change

 -- Scott Kitterman <scott@kitterman.com>  Wed, 13 Jun 2012 01:10:28 -0400

dkimpy (0.5.1-1) unstable; urgency=low

  * New upstream release
    - Rename source to match new upstream name
    - Updated debian/copyright
  * Remove no longer used quilt patch system artifacts

 -- Scott Kitterman <scott@kitterman.com>  Fri, 03 Feb 2012 19:35:05 -0500

pydkim (0.5-1) unstable; urgency=low

  * New upstream release
    - Drop debian/patches/adjust-setup.py.patch, essential parts incorporated
      upstream
    - Remove the no longer existing (and never very useful) dkimsend.sh from
      debian/docs

 -- Scott Kitterman <scott@kitterman.com>  Wed, 26 Oct 2011 17:39:45 -0400

pydkim (0.4.2-1) unstable; urgency=low

  * New upstream release
    - Drop debian/patches/dns-namespace.patch, incorporated upstream

 -- Scott Kitterman <scott@kitterman.com>  Fri, 17 Jun 2011 08:36:05 -0500

pydkim (0.4.1-1) unstable; urgency=low

  * New upstream release
    - Drop debian/patches/fix-key-record-validation.patch and
      relaxed-canonicalization.patch, incorporated upstream
    - Update debian/patches/adjust-setup.py.patch to match upstream setup.py
      versions
    - Add debian/patches/dns-namespace.patch to deconflict dns (python-
      dnspython) and dkim/dns.py namespaces so the package works with either
      python-dns (DNS) or python-dnspython (dns)
    - Drop debian/dkimsign.1 and dkimverify.1, provided in the upstream
      tarball now
    - Drop debian/manpages, installed using upstream setup.py
    - Add depends on python-dns and use python-dnspython as an alternate
    - Increase minimum python version to 2.6 (X-P-V) and add X-Python3-Version
      >= 3.1 for python3 support
    - Add python3-dkim to debian/control for python3 support
      - Only python3-dns is available for python3, so use this
      - Add python3 to build-depends
      - Rework debian/rules to build for python3
    - Update debian/copyright
  * Update debian/watch and debian/control Homepage: to point at the new
    upstream location
  * python-dkim Breaks instead of Conflicts dkimproxy

 -- Scott Kitterman <scott@kitterman.com>  Thu, 16 Jun 2011 15:01:59 -0500

pydkim (0.3-6) unstable; urgency=low

  * Rebuild for python transition
  * Bump Standards-Version to 3.9.2 without futher change
  * Drop Breaks: ${python:Breaks} since it is no longer used

 -- Scott Kitterman <scott@kitterman.com>  Wed, 20 Apr 2011 00:12:53 -0400

pydkim (0.3-5) unstable; urgency=low

  * Add debian/patches/relaxed-canonicalization.patch to fix body hash
    verification failures due to out of sequence operations.  Thanks to
    Martin Pool for the patch
  * Add debian/patches/fix-key-record-validation.patch to fix incorrect key
    record validation failures due to invalid assumptions about public key
    record requirements
  * Bump standards version to 3.9.1 without further change
  * Change XS-Python-Version to X-Python-Version
  * Add Breaks: ${python:Breaks}
  * Drop XB-Python-Version

 -- Scott Kitterman <scott@kitterman.com>  Mon, 07 Mar 2011 00:09:56 -0500

pydkim (0.3-4) unstable; urgency=low

  * Convert from CDBS to Debhelper 7:
    - Drop cdbs from build-depends and bump required debhelper verion
    - Change compat to 7
    - Change debian/rules to DH 7 tiny --with quilt
    - Add quilt to build-depends
    - Update README.source for quilt
    - Remove autogenerated pycompat
  * Convert from python-central to dh_python2
    - Drop python-central from build-depends
    - Build --with python2
    - Bump python version requirement to 2.6.5-2~
  * Bump standards version to 3.8.4 without further change
  * Remove DM-Upload-Allowed (no longer needed)
  * Update debian/dkimsign.1 and debian/dkimverify.1 to specify their license
  * Use correct © symbol in debian/copyright
  * Remove pointless debian/examples file
  * Fix typo in previous debian/changelog entry to make lintian happy

 -- Scott Kitterman <scott@kitterman.com>  Mon, 21 Jun 2010 23:20:36 -0400

pydkim (0.3-3) unstable; urgency=low

  * Adjust dkimproxy conflict to << 1.0.1-8.1 due to filename collision
    (Closes: #511037)
  * Add ${misc:Depends}

 -- Scott Kitterman <scott@kitterman.com>  Mon, 12 Jan 2009 23:17:26 -0500

pydkim (0.3-2) unstable; urgency=low

  * Conflict dkimproxy << 1.0.1-8 due to filename collision
    (Closes: #509045)

 -- Scott Kitterman <scott@kitterman.com>  Fri, 19 Dec 2008 14:34:10 -0500

pydkim (0.3-1) unstable; urgency=low

  * Initial Debian package (Closes: #502264)
  * Add adjust-setup.py.patch to correct shebang in setup.py and to
    not install dkimsend.sh in /usr/bin (at best it's an example and not
    suitable for actual use - will install with docs)

 -- Scott Kitterman <scott@kitterman.com>  Thu, 06 Nov 2008 23:53:21 -0500
